<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class OnePerDay implements Rule
{
    public function passes($attribute, $value)
    {
        return strtoupper($value) === $value;
    }

    public function message()
    {
        return 'More than one registration per day is not allowed.';
    }
}
