<?php

namespace App\Http\Controllers;

use App\Model\Athlete;
use Illuminate\Http\Request;
use App\Http\Requests\StoreAthleteRequest;

class AthleteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $athletes = Athlete::get();
        return response()->json($athletes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAthleteRequest $request)
    {
        $athlete = Athlete::create($request->all());
        return response()->json(['message' => 'Athlete created.', 'data' => $athlete], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Athlete  $athlete
     * @return \Illuminate\Http\Response
     */
    public function show(Athlete $athlete)
    {
        return response()->json(['message' => 'Athelete found.', 'data' => $athlete], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Athlete  $athlete
     * @return \Illuminate\Http\Response
     */
    public function destroy(Athlete $athlete)
    {
        try {
            $athlete->delete();
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 409);
        }
        return response()->json(['message' => 'Athelete deleted.'], 204);
    }
}
