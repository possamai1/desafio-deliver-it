<?php

namespace App\Http\Controllers;

use App\Model\Race;
use App\Model\Athlete;
use DB;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\StoreRaceRequest;
use App\Http\Requests\ResultStoreRequest;

class RaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $races = Race::get();
        return response()->json($races);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRaceRequest $request)
    {
        $race = Race::create($request->all());
        return response()->json(['message' => 'Race created.', 'data' => $race], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function show(Race $race)
    {
        return response()->json(['message' => 'Race found.', 'data' => $race], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function destroy(Race $race)
    {
        try {
            $race->delete();
        } catch (\Throwable $th) {
            return response()->json(['message' => 'Violação entre relacionamento.', 'error' => $th->getMessage()], 409);
        }
        return response()->json(['message' => 'Race deleted.'], 204);
    }

    public function subscription(Request $request, Race $race, Athlete $athlete)
    {
        try {
            if ($athlete->races->where('date', '=', $race->date)->count() > 0) {
                throw new \Exception('Athlete is already enrolled in race on the same day.');
            }

            $race->subscriptions()->save($athlete);
        } catch (\Throwable $th) {
            return response()->json(['message' => 'Violação entre relacionamento.', 'error' => $th->getMessage()], 409);
        }
        return response()->json(['message' => 'Subscription created.', 'data' => $race], 200);
    }

    public function result(ResultStoreRequest $request, Race $race, Athlete $athlete)
    {
        try {
            $race->results()->save($athlete, $request->only('time_start', 'time_end'));
        } catch (\Throwable $th) {
            return response()->json(['message' => 'Violação entre relacionamento.', 'error' => $th->getMessage()], 409);
        }
        return response()->json(['message' => 'Subscription created.', 'data' => $race], 200);
    }

    public function classificationByAge()
    {
        $athletes = DB::table('athletes')
            // ->from(DB::RAW('athletes, (select @rank := 0) t2'))
            ->join('results', 'athletes.id', '=', 'results.athlete_id')
            ->leftJoin('races', 'races.id', '=', 'results.race_id')
            ->select(
                'races.id AS race_id',
                'races.type AS race_type',
                'races.date AS race_date',
                'athletes.id AS athlete_id',
                'athletes.name AS athlete_name',
                DB::RAW('TIMESTAMPDIFF(YEAR, DATE(athletes.birthday), current_date) AS athlete_age'),
                DB::RAW('TIMEDIFF(results.time_start, results.time_end) AS athlete_time'),
            )
            ->orderBy('type')
            ->orderBy('date', 'DESC')
            ->orderBy(DB::RAW('TIMEDIFF(results.time_start, results.time_end)', 'DESC'))
            ->get();
        return response()->json($athletes);
    }

    public function classification(Race $race)
    {
        $athletes = DB::table('athletes')
            ->join('results', 'athletes.id', '=', 'results.athlete_id')
            ->leftJoin('races', 'races.id', '=', 'results.race_id')
            ->select(
                'races.id AS race_id',
                'races.type AS race_type',
                'races.date AS race_date',
                'athletes.id AS athlete_id',
                'athletes.name AS athlete_name',
                DB::RAW('TIMESTAMPDIFF(YEAR, DATE(athletes.birthday), current_date) AS athlete_age'),
                DB::RAW('TIMEDIFF(results.time_start, results.time_end) AS athlete_time'),
            )
            ->orderBy('type')
            ->orderBy('date', 'DESC')
            ->orderBy(DB::RAW('TIMEDIFF(results.time_start, results.time_end)', 'DESC'))
            ->get();
        return response()->json($athletes);
    }
}
