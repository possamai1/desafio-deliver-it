<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResultStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'time_start' => 'required|date_format:H:i:s',
            'time_end' => 'required|date_format:H:i:s',
        ];
    }

    public function attributes()
    {
        return [
            'time_start' => 'tempo inicial',
            'time_end' => 'tempo final',
        ];
    }
}
