<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAthleteRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required', 'min:4'],
            'cpf' => 'required|unique:App\Model\Athlete,cpf|cpf',
            'birthday' => 'required|date_format:Y-m-d|before:-18 years',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nome',
            'cpf' => 'cpf',
            'birthday' => 'aniversário',
        ];
    }
}
