<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRaceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type' => ['required', 'min:2'],
            'date' => 'required|date_format:Y-m-d',
        ];
    }

    public function attributes()
    {
        return [
            'type' => 'tipo',
            'date' => 'data',
        ];
    }
}
