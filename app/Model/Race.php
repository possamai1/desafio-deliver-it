<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    protected $fillable = [
        'type', 'date'
    ];

    public function subscriptions()
    {
        return $this->belongsToMany('App\Model\Athlete', 'athlete_race')->withTimestamps();
    }

    public function results()
    {
        return $this->belongsToMany('App\Model\Athlete', 'results')
                    ->withPivot(['time_start', 'time_end'])
                    ->withTimestamps();
    }
}
