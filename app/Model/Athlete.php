<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Athlete extends Model
{
    protected $fillable = [
        'name', 'cpf', 'birthday'
    ];

    public function races()
    {
        return $this->belongsToMany('App\Model\Race')->withTimestamps();
    }

    public function results()
    {
        return $this->belongsToMany('App\Model\Race')->withTimestamps();
    }
}
