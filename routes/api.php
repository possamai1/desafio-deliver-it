<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'api',], function ($router) {

    Route::resource('athletes', 'AthleteController')->only([
        'index', 'store', 'destroy', 'show'
    ]);


    Route::get('races/classification-by-age', 'RaceController@classificationByAge');
    Route::get('races/classification', 'RaceController@classification');
    Route::resource('races', 'RaceController')->only([
        'index', 'store', 'destroy', 'show'
    ]);
    Route::post('races/{race}/athlete/{athlete}/subscription', 'RaceController@subscription');
    Route::post('races/{race}/athlete/{athlete}/result', 'RaceController@result');
});
