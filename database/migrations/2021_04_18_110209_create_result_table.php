<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->primary(['athlete_id','race_id']);
            $table->bigInteger('athlete_id')->unsigned();
            $table->bigInteger('race_id')->unsigned();
            $table->time('time_start');
            $table->time('time_end');
            $table->timestamps();

            $table->foreign('athlete_id')
                ->references('id')
                ->on('athletes');
            $table->foreign('race_id')
                ->references('id')
                ->on('races');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
